pipeline {
  agent any // Replace by specific label for narrowing down to OutSystems pipeline-specific agents. A dedicated agent will be allocated for the entire pipeline run.
  parameters {
    // Pipeline parameters are automatically filled by LT Trigger plugin
    string(name: 'TriggerManifest', defaultValue: '', description: 'Trigger manifest artifact (in JSON format) for the current pipeline run.')
    string(name: 'TriggeredBy', defaultValue: 'N/A', description: 'Name of LifeTime user that triggered the pipeline remotely.')
  }
  options { skipStagesAfterUnstable() }
  environment {
     // Artifacts Folder
    ArtifactsFolder = "Artifacts"
    // LifeTime Specific Variables
    LifeTimeHostname = 'showcase-lt.outsystemsdevopsexperts.com'
    LifeTimeAPIVersion = 2
    // Authentication Specific Variables
    AuthorizationToken = credentials('LifeTimeServiceAccountToken_Showcase')
    ADHostname = "archpp.outsystems.com"
    ADActivationCode = "Y4B.U3L.IEY.OSR.VNB.AZS.E1I.QUT"
    AIMentor_API_Key = credentials("AIMentor_Api_Key")
    // Environments Specification Variables
    /*
    * Pipeline for 5 Environments:
    *   DevelopmentEnvironment    -> Where you develop your applications. This should be the default environment you connect with service studio.
    *   RegressionEnvironment     -> Where your automated tests will test your applications.
    *   AcceptanceEnvironment     -> Where you run your acceptance tests of your applications.
    *   PreProductionEnvironment  -> Where you prepare your apps to go live.
    *   ProductionEnvironment     -> Where your apps are live.
    */
    DevelopmentEnvironmentLabel = 'Development'
    RegressionEnvironmentLabel = 'Regression' //consider this as acceptance in scoot
   // AcceptanceEnvironmentLabel = 'Acceptance'
   // PreProductionEnvironmentLabel = 'Pre-Production'
    ProductionEnvironmentLabel = 'Production'
    // Regression URL Specification
    ProbeEnvironmentURL = 'https://showcase-reg.outsystemsdevopsexperts.com/'
    BddEnvironmentURL = 'https://showcase-reg.outsystemsdevopsexperts.com/'
    // Test Management API Endpoints
    RegressionTestJobURL = 'https://showcase-dev.outsystemsdevopsexperts.com/TestManagement_CS/rest/v1/TriggerTestRun?ReportType=junit&Env=REG&JobKey=demo-reg-job'
    // OutSystems PyPI package version
    OSPackageVersion = '0.5.0'
  }
  stages {
    stage('Install Python Dependencies') {
      steps {
        // Create folder for storing artifacts
        sh script: "mkdir ${env.ArtifactsFolder}", label: 'Create artifacts folder'
        // Write trigger manifest content to a file
        writeFile file: "${env.ArtifactsFolder}/trigger_manifest.cache", text: "${params.TriggerManifest}"
        // Only the virtual environment needs to be installed at the system level
        sh script: 'pip3 install -q -I virtualenv --user', label: 'Install Python virtual environments'
        // Install the rest of the dependencies at the environment level and not the system level
        withPythonEnv('python3') {
          sh script: "pip3 install -U outsystems-pipeline==\"${env.OSPackageVersion}\"", label: 'Install required packages'
        }
      }
    }

    stage('Get Test App Lists') {
      steps { 
        script{
          env.ApplicationScopeWithTests = ''
          def concanAppTestList = ""
          def object = readJSON file: "${env.ArtifactsFolder}/trigger_manifest.cache"
          object.ApplicationVersions.each{param -> 
            if (param.IsTestApplication)
            {
              concanAppTestList = param.ApplicationName + concanAppTestList + ","
            }
          }
          env.ApplicationScopeWithTests = concanAppTestList.substring(0, concanAppTestList.length() - 1)
          echo "$env.ApplicationScopeWithTests"
		    }	  
      }
    }
    
    stage('Get and Deploy Latest Tags') {
      steps {        
        echo "Pipeline run triggered remotely by '${params.TriggeredBy}'. Check trigger manifest artifact for additional details."
        withPythonEnv('python3') {
          // Deploy the application list, with tests, to the Acceptance environment
          lock('deployment-plan-REG') {
            sh script: "python3 -m outsystems.pipeline.deploy_tags_to_target_env_with_manifest --artifacts \"${env.ArtifactsFolder}\" --lt_url ${env.LifeTimeHostname} --lt_token ${env.AuthorizationToken} --lt_api_version ${env.LifeTimeAPIVersion} --source_env_label \"${env.DevelopmentEnvironmentLabel}\" --destination_env_label \"${env.RegressionEnvironmentLabel}\" --include_test_apps --manifest_file \"${env.ArtifactsFolder}/trigger_manifest.cache\"", label: "Deploy latest application tags (including tests) to ${env.RegressionEnvironmentLabel}"
          }
          // Apply configuration values to Acceptance environment, if any
          sh script: "python3 -m outsystems.pipeline.apply_configuration_values_to_target_env --artifacts \"${env.ArtifactsFolder}\" --lt_url ${env.LifeTimeHostname} --lt_token ${env.AuthorizationToken} --target_env_label \"${env.RegressionEnvironmentLabel}\" --manifest_file \"${env.ArtifactsFolder}/trigger_manifest.cache\"", label: "Apply values to configuration items in ${env.RegressionEnvironmentLabel}"
        }
      }
    }
    stage('Validate Tech Debt') {
      steps {     
        withPythonEnv('python3') {
          // check AD tech debt
          sh script :"python3 -m outsystems.pipeline.fetch_tech_debt --artifacts \"${env.  ArtifactsFolder}\" --ad_hostname ${env.ADHostname} --activation_code ${env.ADActivationCode} --api_key ${env.AIMentor_API_Key} --manifest_file \"${env.ArtifactsFolder}/trigger_manifest.cache\""
          sh script :"python3 -u techdebt_validation.py -m \"${env.ArtifactsFolder}/trigger_manifest.cache\" -d \"${env.ArtifactsFolder}/techdebt_data/\"", returnStatus: true, label: 'Run tech debt validation'
        }        
      }
    }

    stage('Accept Tech Debt and proceed?') {
      steps {
        // Define milestone before approval gate to manage concurrent builds
        milestone(ordinal: 40, label: 'before-approval')
        // Wrap the confirm option in a timeout to avoid hanging Jenkins forever
        timeout(time:1, unit:'DAYS') {
          input 'Accept tech debt and run regression?'
        }
        // Discard previous builds that have not been accepted yet
        milestone(ordinal: 50, label: 'after-approval')
      }
    }

    stage('Run Regression') {
      //when {
        // Checks if there are any test applications in scope before running the regression stage
  
        //expression { return params.ApplicationScope != params.ApplicationScopeWithTests }
     // }
      steps {
        withPythonEnv('python3') {
          // Generate the URL endpoints of the BDD tests
          sh script: "python3 -m outsystems.pipeline.generate_unit_testing_assembly --artifacts \"${env.ArtifactsFolder}\" --app_list \"${env.ApplicationScopeWithTests}\" --cicd_probe_env ${env.ProbeEnvironmentURL} --bdd_framework_env ${env.BddEnvironmentURL}", label: 'Generate URL endpoints for BDD test suites'
          // Run those tests and generate a JUnit test report
          sh script: "python3 -m outsystems.pipeline.evaluate_test_results --artifacts \"${env.ArtifactsFolder}\"", returnStatus: true, label: 'Run BDD test suites and generate JUnit test report'          
        }
      }
      post {
        always {
          // Publish results in JUnit test report
          junit testResults: "${env.ArtifactsFolder}/junit-result.xml", allowEmptyResults: true, skipPublishingChecks: true
        }
      }
    }
    //stage('Deploy Acceptance') {
    //  steps {     
    //    withPythonEnv('python3') {
          // Deploy the application list to the Acceptance environment
    //      lock('deployment-plan-ACC') {
    //        sh script: "python3 -m outsystems.pipeline.deploy_tags_to_target_env_with_manifest --artifacts \"${env.ArtifactsFolder}\" --lt_url ${env.LifeTimeHostname} --lt_token ${env.AuthorizationToken} --lt_api_version ${env.LifeTimeAPIVersion} --source_env_label \"${env.RegressionEnvironmentLabel}\" --destination_env_label \"${env.AcceptanceEnvironmentLabel}\" --manifest_file \"${env.ArtifactsFolder}/trigger_manifest.cache\"", label: "Deploy latest application tags to ${env.AcceptanceEnvironmentLabel}"
    //      }
          // Apply configuration values to Regression environment, if any
    //      sh script: "python3 -m outsystems.pipeline.apply_configuration_values_to_target_env --artifacts \"${env.ArtifactsFolder}\" --lt_url ${env.LifeTimeHostname} --lt_token ${env.AuthorizationToken} --target_env_label \"${env.AcceptanceEnvironmentLabel}\" --manifest_file \"${env.ArtifactsFolder}/trigger_manifest.cache\"", label: "Apply values to configuration items in ${env.AcceptanceEnvironmentLabel}"
    //    }        
    //  }
   // }
    stage('Accept Changes') {
    steps {
        // Define milestone before approval gate to manage concurrent builds
       milestone(ordinal: 60, label: 'before-approval')
        // Wrap the confirm option in a timeout to avoid hanging Jenkins forever
        timeout(time:1, unit:'DAYS') {
          input 'Accept changes and deploy to Production?'
        }
        // Discard previous builds that have not been accepted yet
        milestone(ordinal: 70, label: 'after-approval')
     }
    }
    //stage('Deploy Dry-Run') {
    //  steps {
    //    withPythonEnv('python3') {
    //      // Deploy the application list to the Pre-Production environment
    //      lock('deployment-plan-PRE') {
    //        sh script: "python3 -m outsystems.pipeline.deploy_tags_to_target_env_with_manifest --artifacts \"${env.ArtifactsFolder}\" --lt_url ${env.LifeTimeHostname} --lt_token ${env.AuthorizationToken} --lt_api_version ${env.LifeTimeAPIVersion} --source_env_label \"${env.AcceptanceEnvironmentLabel}\" --destination_env_label \"${env.PreProductionEnvironmentLabel}\" --manifest_file \"${env.ArtifactsFolder}/trigger_manifest.cache\"", label: "Deploy latest application tags to ${env.PreProductionEnvironmentLabel}"
    //      }
          // Apply configuration values to PreProduction environment, if any
    //      sh script: "python3 -m outsystems.pipeline.apply_configuration_values_to_target_env --artifacts \"${env.ArtifactsFolder}\" --lt_url ${env.LifeTimeHostname} --lt_token ${env.AuthorizationToken} --target_env_label \"${env.PreProductionEnvironmentLabel}\" --manifest_file \"${env.ArtifactsFolder}/trigger_manifest.cache\"", label: "Apply values to configuration items in ${env.PreProductionEnvironmentLabel}"
    //    }
    //  }
    //}
    stage('Deploy Production') {
      steps {
        withPythonEnv('python3') {
          // Deploy the application list to the Production environment
          lock('deployment-plan-PRD') {
            sh script: "python3 -m outsystems.pipeline.deploy_tags_to_target_env_with_manifest --artifacts \"${env.ArtifactsFolder}\" --lt_url ${env.LifeTimeHostname} --lt_token ${env.AuthorizationToken} --lt_api_version ${env.LifeTimeAPIVersion} --source_env_label \"${env.RegressionEnvironmentLabel}\" --destination_env_label \"${env.ProductionEnvironmentLabel}\" --manifest_file \"${env.ArtifactsFolder}/trigger_manifest.cache\"", label: "Deploy latest application tags to ${env.ProductionEnvironmentLabel}"
          }
          // Apply configuration values to Production environment, if any
          sh script: "python3 -m outsystems.pipeline.apply_configuration_values_to_target_env --artifacts \"${env.ArtifactsFolder}\" --lt_url ${env.LifeTimeHostname} --lt_token ${env.AuthorizationToken} --target_env_label \"${env.ProductionEnvironmentLabel}\" --manifest_file \"${env.ArtifactsFolder}/trigger_manifest.cache\"", label: "Apply values to configuration items in ${env.ProductionEnvironmentLabel}"
        }
      }
    }
  }
  post {
    // It will always store the cache files generated, for observability purposes, and notifies the result
    always {
      dir ("${env.ArtifactsFolder}") {
        archiveArtifacts artifacts: "**/*.cache"
      }
    }
    // If there's a failure, tries to store the Deployment conflicts (if exists), for troubleshooting purposes
    failure {
      dir ("${env.ArtifactsFolder}") {
        archiveArtifacts artifacts: 'DeploymentConflicts', allowEmptyArchive: true
      }
    }
    // Delete artifacts folder content
    cleanup {      
      dir ("${env.ArtifactsFolder}") {
        deleteDir()
      }
    }
  }
}
